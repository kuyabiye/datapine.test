var APP = APP || {};
define(["ko", "jquery", "highcharts", "APP.utils"], function(ko, $, highcharts) {

  APP.Chart = function(view, options) {
      this.view = view;
      this.options = $.extend({}, this.constructor.DEFAULTS, options);

      this.init();
  }
  
  APP.Chart.DEFAULTS = {
    type: "line",
    title: "Graph"
  };      

  APP.Chart.prototype.init = function() {
    var self = this;

    this.title = ko.observable(this.options.title || "");
    this.url = ko.observable( APP.utils.getUrlParameters("url") || this.options.url);

    $.getJSON(this.url(), function(json) {
      self.data = json;
      self.highcharts = self.draw(self.data);
    });

    this.type = ko.observable(this.options.type || "line");

    this.type.subscribe(function() {
      self.draw();
    });

    ko.applyBindings(this, this.view);

    $(this.view).data("instance", this);
  }

  APP.Chart.prototype.draw = function(json) {
    var self = this;

    return $(self.view).find(".chart").highcharts({
        chart: {
          type: self.type()
        },
        title: {
          text: self.options.title || ""
        },
        series: json || self.data
      })
  };

  return APP.Chart;

});
