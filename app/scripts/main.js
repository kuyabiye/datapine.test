/*global require*/
'use strict';

require.config({
  paths: {
    jquery: '../bower_components/jquery/jquery',
    highcharts: "../bower_components/highcharts-release/highcharts",
    ko: "../bower_components/knockout/dist/knockout"
  },
  shim: {
      highcharts: {
        deps: ["jquery"],
        exports: 'highcharts'
      }    
    }
});


var APP = APP || {};

APP.init = function (context) {
  var _context = context || document.body;

  var views = _context.querySelectorAll(".js-require");

  [].forEach.call(views, function(view) {
    var data = view.dataset;
    if ( data.model ) {
      require([data.model], function(Model) {
        new Model(view, view.dataset || {});
      })
    };
  });

}


APP.init();
